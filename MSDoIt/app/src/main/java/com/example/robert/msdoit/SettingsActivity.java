package com.example.robert.msdoit;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.provider.Settings;

/* Created by Robert Neumann on 30.04.2018.
 * Matrikel-Nr.: 15589
 * ToDo:
 */public class SettingsActivity extends PreferenceActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings); // This is way shorter than a PreferenceFragment and Manager
    }

}
