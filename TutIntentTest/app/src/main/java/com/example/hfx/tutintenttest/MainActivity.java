package com.example.hfx.tutintenttest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

   // Button btnDownload = findViewById(R.id.btnDownload);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void clickEvent(View v)
    {
        Intent intent = new Intent(this, IntentServiceTut.class);
        startService(intent);
    }
}
