package com.example.hfx.tutintenttest;

        import android.app.IntentService;
        import android.content.Intent;
        import android.os.Vibrator;
        import android.support.annotation.Nullable;
        import android.util.Log;
        import android.widget.Toast;

public class IntentServiceTut extends IntentService {

    private static final String TAG = "MeinPaket";
    public IntentServiceTut(){
        super("IntentServiceTut");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //Das macht der Service

        Log.i(TAG,"Service works!");
    }
}

