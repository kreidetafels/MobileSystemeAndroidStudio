package com.example.hfx.tuttextviewfromservice;

//https://stackoverflow.com/questions/23968240/updating-activity-textview-from-service-using-service-binding
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView textView = findViewById(R.id.main_activity_text_view);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        double latitude = intent.getDoubleExtra(LocationBroadcastService.EXTRA_LATITUDE, 0);
                        double longitude = intent.getDoubleExtra(LocationBroadcastService.EXTRA_LONGITUDE, 0);
                        textView.setText("Lat: " + latitude + ", Lng: " + longitude);
                    }
                }, new IntentFilter(LocationBroadcastService.ACTION_LOCATION_BROADCAST)
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, LocationBroadcastService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, LocationBroadcastService.class));
    }
}
