package com.example.hfx.msinsomnia;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.support.v4.app.DialogFragment;
import java.util.Calendar;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * ToDo:
 * */

public class TimePickerFragment extends DialogFragment {

    PendingIntent pendingIntent;
    AlarmManager alarmManager;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);



        //getActivity() => Context, onTimeSetListener --> Wohin wird die Zeit gesendet? => Cast, um es an die unterliegene Activity zu senden!
        return new TimePickerDialog(getActivity(), (TimePickerDialog.OnTimeSetListener)getActivity(), hour, minute, DateFormat.is24HourFormat(getActivity())); //is24HourFormat(Kontext)
    }
}
