package com.example.hfx.msinsomnia;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Calendar;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * ToDo:
 * */

public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    private TextView textView;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Calendar c = Calendar.getInstance();

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;
        Button btnTimePicker = findViewById(R.id.btnTimepicker);
        Button btnSetAlarm = findViewById(R.id.btnSetAlarm);
        Button btnCancelAlarm = findViewById(R.id.btnCancelAlarm);
        TextView textView = findViewById(R.id.tVTime);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        //Intent erstellen fuer den Alarm

        Intent intentAlarm = new Intent(context, AlarmReceiver.class);
        TextView text = (TextView) findViewById(R.id.tVTime);
        Toast.makeText(this, "Alarm auf: "+ text.getText().toString() + " gesetzt", Toast.LENGTH_SHORT).show();

        //Pending Intent, um den Intent zu verzoegern bis die Zeit angegeben wurde
        pendingIntent = PendingIntent.getBroadcast(this, 0, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        TextView textView = findViewById(R.id.tVTime);
        textView.setText(hourOfDay + ":"+ minute + " Uhr");
    }

    void openTimepicker(View v) {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "Time Picker");
    }

    void setAlarm(View v) {

        //AlarmManager setzen
//         alarmManager.set(AlarmManager.RTC_WAKEUP,5000, pendingIntent);
            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+5000, pendingIntent);
    }

    void cancelAlarm(View v) {
        Toast.makeText(this, "Alarm nicht gesetzt", Toast.LENGTH_SHORT).show();
    }
}
