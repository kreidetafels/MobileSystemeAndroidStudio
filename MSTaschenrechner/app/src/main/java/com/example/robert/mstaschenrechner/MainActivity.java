/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * ToDo:
 * */

package com.example.robert.mstaschenrechner;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;


/*
*
* ToDo: Zwischenergebnis zeigt manchmal nicht ganz das richtige an; Menuitem muessen noch Error resetten*/

public class MainActivity extends AppCompatActivity {


    TextView tvErgebnis;
    TextView tvZwischenergebnis;

    boolean boolErgebnis = false;


    //Wie klappt das?
    private static String getSizeName(Context context) {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large";
            case 4: // Configuration.SCREENLAYOUT_SIZE_XLARGE is API >= 9
                return "xlarge";
            default:
                return "undefined";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvErgebnis = findViewById(R.id.tVErgebnis);
        tvZwischenergebnis = findViewById(R.id.tVZwischenerg);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putString("ergebnis", (String) tvErgebnis.getText());
        outState.putString("zwischenergebnis", (String) tvZwischenergebnis.getText());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        tvErgebnis.setText(savedInstanceState.getString("ergebnis"));
        tvZwischenergebnis.setText(savedInstanceState.getString("zwischenergebnis"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.commonmenus,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        btnMenuItems(item);
        return super.onOptionsItemSelected(item);
    }


    //Zahlen & Zeichen
    void onClickOperand(View v) {
        if(!boolErgebnis){
            tvErgebnis.setText("");
            tvZwischenergebnis.setText("");
            boolErgebnis = true;
        }
        btnToTvErgebnis(v);
    }

    //+ - * /
    void onClickOperator(View v){
    //    boolErgebnis = false;
        if(!boolErgebnis){
            tvErgebnis.setText("");
            tvZwischenergebnis.setText("");
            boolErgebnis = true;
        }
        btnToTvErgebnis(v);
    }


    //Alle Funktionen wie Clear, Backspace, Result
    void onClickBtnFunctions(View v) {
        switch (v.getId()) {
            case R.id.btnClear: {
                tvErgebnis.setText("");
                tvZwischenergebnis.setText("");
                break;
            }
            case R.id.btnBackspace: {
                tvErgebnis.setText(removeLastChar(tvErgebnis.getText().toString()));
                break;
            }
            case R.id.btnResult: {

                String wurzel = getString((R.string.sqr_root));
                String zwischenrechnung = tvErgebnis.getText().toString().replaceAll(wurzel,"sqrt");

                tvZwischenergebnis.setText(tvErgebnis.getText());

                if (!zwischenrechnung.isEmpty()) {

                    tvZwischenergebnis.append(zwischenrechnung.replaceAll("sqrt",wurzel + " ="));

                    if (!boolErgebnis) {
                        tvErgebnis.setText("");
                    }
                    try {
                        Expression exp = new ExpressionBuilder(zwischenrechnung).build();
                        tvErgebnis.setText(Double.toString(exp.evaluate()));
                        boolErgebnis = true;


                    } catch (RuntimeException e) {
                        tvErgebnis.setText("Error");
                        tvZwischenergebnis.setText("");
                        boolErgebnis = false;
                    }
                }
            else{
                    //ToDo?
                tvErgebnis.setText("jo");}
            }
        }
    }

    private static String removeLastChar(String str) {
        if(str.length()>0) {
            return str.substring(0, str.length() - 1);
        }
        else
            return str;
    }


    //Bringt btnText ins TextView
    private void btnToTvErgebnis(View v)
    {
  //      tvErgebnis.setText((String) (tvErgebnis.getText() + (String) ((Button) findViewById(v.getId())).getText()));

        tvErgebnis.setText((String) (tvErgebnis.getText() + (String) ((Button) findViewById(v.getId())).getText()));
        switch(v.getId()){
            case R.id.btnSin: {
                tvErgebnis.append((String) "(");
                break;
            }
            case R.id.btnCos: {
                tvErgebnis.append((String) "(");
                break;
            }
            case R.id.btnTan: {
                tvErgebnis.append((String) "(");
                break;
            }
        }
    };

    //Sin, Tan, Cos, Sqrt Klammern setzen

    //Zusatzfunktionen für Sin, Cos, Tan  & Sqrt
    private void btnMenuItems(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itmSin: {
                tvErgebnis.append((String) (getString(R.string.sin)+ tvErgebnis.getText()));
                break;

            }
            case R.id.itmCos: {
                tvErgebnis.append((String) (getString(R.string.cos) + tvErgebnis.getText()));
                break;
            }
            case R.id.itmTan: {
                tvErgebnis.append((String) (getString(R.string.tan)+ tvErgebnis.getText()));
                break;
            }
            case R.id.itmSqrt: {
            //    tvErgebnis.setText((String) (getString(R.string.sqr_root)+"("+ tvErgebnis.getText()));
                tvErgebnis.append((String) (getString(R.string.sqr_root)+"("));
                break;
            }
        }
    }
}