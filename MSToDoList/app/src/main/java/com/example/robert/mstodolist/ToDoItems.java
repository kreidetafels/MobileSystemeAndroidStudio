package com.example.robert.mstodolist;

import java.io.Serializable;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * */

    public class ToDoItems implements Serializable{
    private String strTitle;
    private String strDescription;
    private String strPriority;

    public ToDoItems(String _titel, String _beschreibung, String _priorität) {
        this.setStrTitle(_titel);
        this.setStrDescription(_beschreibung);
        this.setStrPriority(_priorität);
    }

    public ToDoItems setStrTitle(String _titel) {
        if (_titel != null && !_titel.isEmpty())
            this.strTitle = _titel;
        return this;
    }

    public ToDoItems setStrDescription(String _beschreibung) {
        if (_beschreibung != null && !_beschreibung.isEmpty())
            this.strDescription = _beschreibung;
        return this;
    }

    public ToDoItems setStrPriority(String _priorität) {
        if (_priorität != null && !_priorität.isEmpty())
            this.strPriority = _priorität;
        return this;
    }

    public String getStrTitle() {
        return this.strTitle;
    }

    public String getStrDescription() {
        return this.strDescription;
    }

    public String getStrPriority() {
        return this.strPriority;
    }

    @Override
    public String toString() {
        return this.getStrTitle();
    }


}
