package com.example.robert.mstodolist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * Hilfe von R. Pfeiffer
 * */

public class Details extends AppCompatActivity {

    public EditText etTitle;
    public EditText eTDescription;
    public Spinner spinPriority;
    public ArrayAdapter<CharSequence> adapter;
    public ToDoItems clickedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        etTitle = findViewById(R.id.eTTitle);
        eTDescription = findViewById(R.id.eTDescription);
        spinPriority = findViewById(R.id.spinner_prio);
        adapter = ArrayAdapter.createFromResource(this, R.array.prioritaet, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPriority.setAdapter(adapter);

        Intent intent = getIntent();
        if (intent.getSerializableExtra("ListItem") != null) {
            clickedItem = (ToDoItems) intent.getSerializableExtra("ListItem");
            etTitle.setText(clickedItem.getStrTitle());
            eTDescription.setText(clickedItem.getStrDescription());
            switch (clickedItem.getStrPriority()) {
                case "Hoch":
                    spinPriority.setSelection(0);
                    break;
                case "Mittel":
                    spinPriority.setSelection(1);
                    break;
                case "Niedrig":
                    spinPriority.setSelection(2);
                    break;
            }
        }
        if(clickedItem==null){
            findViewById(R.id.btnDel).setVisibility(View.INVISIBLE);
        }else{
            findViewById(R.id.btnDel).setVisibility(View.VISIBLE);
        }

    }

    public void addItemToList(View v) {
        if (!etTitle.getText().toString().isEmpty()) {
            if(eTDescription.getText().toString().isEmpty())
                eTDescription.setText(" ");
            Intent result = new Intent();
            result.putExtra("DETAIL_RESULT", new ToDoItems(etTitle.getText().toString(), eTDescription.getText().toString(), spinPriority.getSelectedItem().toString()));

            if (clickedItem != null) {
                result.putExtra("DETAIL_REQUEST", getIntent().getIntExtra("ListPosition",-1));
                setResult(MainActivity.REG_EDIT, result);
            } else {
                setResult(MainActivity.REQ_ADD, result);
            }
            finish();
        }else{
            Toast.makeText(this,"Bitte Titel eingeben", Toast.LENGTH_SHORT).show();
        }
    }

    public void removeItemFromList(View v){
        Intent result = new Intent();
        result.putExtra("DETAIL_REQUEST", getIntent().getIntExtra("ListPosition",-1));
        setResult(MainActivity.REG_DEL, result);
        finish();
    }


}
