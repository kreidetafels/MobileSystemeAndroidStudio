package com.example.robert.mstodolist;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * */

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sharedprefs);
    }
}