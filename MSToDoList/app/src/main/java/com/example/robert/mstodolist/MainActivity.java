package com.example.robert.mstodolist;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * Hilfe von R. Pfeiffer
 * */

public class MainActivity extends ListActivity {

    public static final int REQ_ADD = 0;
    public static final int REG_EDIT = 1;
    public static final int REG_DEL = 2;

    public ArrayAdapter<ToDoItems> arrayAdapter;
   // public ArrayList<ToDoItems> toDoListe = new ArrayList<ToDoItems>();
    public TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arrayAdapter = new ArrayAdapter<ToDoItems>(this, android.R.layout.simple_list_item_1) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                /// Get the Item from ListView
                View view = convertView;

                view = super.getView(position, convertView, parent);
                tv = (TextView) view.findViewById(android.R.id.text1);
                // Set the text size 20 dip or preferenceValue for ListView each item
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, changeFontsize());
                // Return the view
                return view;
            }
        };

        setListAdapter(arrayAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menuAddItem) {
            startActivityForResult(new Intent(this, Details.class), REQ_ADD);
            return true;
        }
        else if (id == R.id.menuSettings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        startActivityForResult(new Intent(this, Details.class).putExtra("ListItem", (ToDoItems) l.getItemAtPosition(position)).putExtra("ListPosition", position), REG_EDIT);
     //   Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int REQUEST_CODE, int RESULT_CODE, Intent data) {

        if (RESULT_CODE == REQ_ADD) {
            arrayAdapter.add((ToDoItems) data.getSerializableExtra("DETAIL_RESULT"));
            Toast.makeText(this, "onactvity", Toast.LENGTH_SHORT).show();
        }else if (RESULT_CODE == REG_EDIT) {
            arrayAdapter.remove(arrayAdapter.getItem(data.getIntExtra("DETAIL_REQUEST", 0)));
            arrayAdapter.add((ToDoItems) data.getSerializableExtra("DETAIL_RESULT"));
        }else if(RESULT_CODE==REG_DEL){
            arrayAdapter.remove(arrayAdapter.getItem(data.getIntExtra("DETAIL_REQUEST", 0)));
        }
    }

    public int changeFontsize(){
    int fontsize = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("schriftgroesse","15"));
    return fontsize;
    }



    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        ArrayList<String> templist = new ArrayList<String>();
        for (int i = 0; i < arrayAdapter.getCount(); i++) {
            templist.add(arrayAdapter.getItem(i).getStrTitle());
            templist.add(arrayAdapter.getItem(i).getStrDescription());
            templist.add(arrayAdapter.getItem(i).getStrPriority());
        }
        savedInstanceState.putStringArrayList("adapter", templist);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (tv != null)
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, changeFontsize());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.getStringArray("adapter") != null) {
            ArrayList<String> templist = savedInstanceState.getStringArrayList("adapter");
            arrayAdapter = new ArrayAdapter<ToDoItems>(this, android.R.layout.simple_list_item_1);
            for (int i = 0; i < templist.size(); i = i + 3) {
                arrayAdapter.add(new ToDoItems(templist.get(i), templist.get(i + 1), templist.get(i + 2)));
            }
        }

    }


}
