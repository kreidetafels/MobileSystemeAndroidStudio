package com.example.hfx.tutsimpleandroidalarmapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnSet;
    EditText tVTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSet = findViewById(R.id.btnSetAlarm);
        tVTime = findViewById(R.id.tVTime);

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int time = Integer.parseInt(tVTime.getText().toString());

                //Intent von MainActivity zur Alarm-Klasse
                Intent i = new Intent(MainActivity.this, Alarm.class);

                //PendingIntent holt sich den BC-Service von dieser App zum Intent i (also AlarmManager)
                PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(),0,i,0);
                //Typecast zum AlarmManager, sonst imkopatibel
                //AM-Objekt erstellen
                AlarmManager aMTime = (AlarmManager)getSystemService(ALARM_SERVICE);

                //Alarm setzen mit RequestCall auf RTC_WakeUp, von jetztiger Zeit + Zeit vom User
                aMTime.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+time*1000,pi);
            }
        }) ;

    }
}
