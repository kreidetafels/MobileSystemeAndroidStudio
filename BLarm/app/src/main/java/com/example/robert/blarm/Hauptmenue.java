package com.example.robert.blarm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class Hauptmenue extends AppCompatActivity {

    private TimePicker uhrzeitStellen;
    private Button btn_ShowTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hauptmenue);
        showTime();
    }

    public void showTime()
    {
        uhrzeitStellen = (TimePicker) findViewById(R.id.uhrzeit);
        btn_ShowTime = (Button) findViewById(R.id.btnShowTime);

        btn_ShowTime.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getBaseContext(), uhrzeitStellen.getCurrentHour() + " : " + uhrzeitStellen.getCurrentMinute(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

}
