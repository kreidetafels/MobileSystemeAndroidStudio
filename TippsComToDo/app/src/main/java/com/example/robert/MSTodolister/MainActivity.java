package com.example.robert.MSTodolister;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageButton button;
    EditText input;
    ListView task_list_view;
    List<Task> tasks;
    TodoListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.add_task_button);
        input =  findViewById(R.id.input_task);
        task_list_view = findViewById(R.id.list_view);
        tasks = new ArrayList<Task>();
        adapter = new TodoListAdapter(tasks, this);
        task_list_view.setAdapter(adapter);
    }



    void btnClickListener(View v) {
        if (input.getText().length() > 0) {

            Toast.makeText(this, input.getText().toString(), Toast.LENGTH_SHORT).show();
            tasks.add(new Task(input.getText().toString(), false));
            adapter.notifyDataSetChanged();
        }
   /*     if (input.getText().length() > 0) {
            Toast.makeText(this, input.getText().toString(), Toast.LENGTH_SHORT).show();

//            tasks.add(new Task(input.getText().toString(), false));
       //     adapter.notifyDataSetChanged();
        }*/
    }
}

