package com.example.hfx.tutadapterviewstodo;

import java.util.ArrayList;

class ToDos {

    private String m_title;
    private String m_description;
    ArrayList<Integer> m_categories;

    ToDos(String title)
    {
        this(title, new String());
    }

    ToDos(String title, String description)
    {
        m_title = title;
        m_description = description;
    }

    public String getTitle()
    {
        return m_title;
    }

    public String getDescription()
    {
        return m_description;
    }


    public ToDos setTitle(String title)
    {
        m_title = title;
        return this;
    }

    public ToDos setDesc(String desc)
    {
        m_description = desc;
        return this;
    }

    public String stringify()
    {
        return m_title + ";" + m_description + ";";
    }

    static ToDos fromString(String str)
    {
        String[] parts = str.split(";");
        if(parts.length == 2)
            return new ToDos(parts[0], parts[1]);
        else
            return new ToDos("");
    }

}
