package com.example.hfx.tutadapterviewstodo;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public ArrayAdapter<ToDos> arrayAdapter;
    public static final int DETAIL_NEW_REQUEST_CODE = 1;
    public static final int DETAIL_EDIT_REQUEST_CODE = 2;
    public static final int DETAIL_DELETE_REQUEST_CODE = 3;
    public static final String TEXT_SIZE = "textsize";
    public ArrayList<ToDos> ToDoListe = new ArrayList<ToDos>();
    public TextView tVText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tVText = findViewById(R.id.tVText);

        };
      //  setListAdapter(arrayAdapter);
   //    arrayAdapter.add(new ToDos("die Bunsmappe abarbeiten", "da fehlen noch einige Aufgaben"));

//        adapter.add(new ErinnerMich("die Bungsmappe abarbeiten", "da fehlen noch einige Aufgaben", "Mittel"));

  //      Toast.makeText(this, "hallo", Toast.LENGTH_LONG).show();


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
