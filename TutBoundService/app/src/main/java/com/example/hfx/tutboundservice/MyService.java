package com.example.hfx.tutboundservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Binder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * ToDo: Dateiname --> für Unterordner?
 * */



public class MyService extends Service {

    private Handler handler = new Handler();
    private int progressNumber;
    protected static boolean cancelDL = false;
    static boolean boolOverWriteDL;

    final static String MY_ACTION = "MY_ACTION";
    final static String INVALID_URL = "INVALID_URL";
    private LocalBroadcastManager mBroadcastManger;

    final static String ServLogTag = "ServiceLogTag";
    Context context; // before onCreate in MainActivity

    //Leerer Konstruktor
    public MyService(){
    }

    public void onDestroy() {
        super.onDestroy();

        //Service schließen, nachdem er seinen Dienst verrichtet hat, sonst Leak in Logcat+
        // vlt. stopSelf?
        stopService(new Intent(this, MyService.class));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendMessage(int _progressNumber) {
        Intent intent = new Intent(MY_ACTION);
        intent.putExtra(MY_ACTION, String.valueOf(_progressNumber));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void noValidURL(String _url) {
        Intent intent = new Intent(INVALID_URL);
        intent.putExtra(INVALID_URL, _url);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        Log.d(ServLogTag,_url);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBroadcastManger = LocalBroadcastManager.getInstance(this);
    }

    //Objekt erstellen, um den Client an den Service zu binden
    private final IBinder iBinderTutorial = new MyLocaleBinder();

    //Wird zu start aufgerufen, was soll er machen, wenn er bindet? --> return iBinderTutorial, was nur eine Instan von MyLocaleBinder ist
    //Android is stupid
    @Override
    public IBinder onBind(Intent intent) {
        return iBinderTutorial;
    }

    public void getDL(String urlFromTextview) {
        final Uri uri = Uri.parse(urlFromTextview);
        final String urlString = uri.toString();


            new Thread(new Runnable() {

                @Override
                public void run() {
                    //Can't create handler inside thread that has not called Looper.prepare()
                    Looper.prepare();
                    int count = 0;
                    try {
                        URL url = new URL(urlString);
                        Uri uri = Uri.parse(urlString);
                        String fileName = uri.getLastPathSegment();

                        //Ordner erstellen
                        boolean mExternalStorageAvailable = false;
                        boolean mExternalStorageWriteable = false;

                        String state = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            mExternalStorageAvailable = true;
                            mExternalStorageWriteable = true;
                        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                            mExternalStorageAvailable = true;
                            mExternalStorageWriteable = false;
                        } else {
                            mExternalStorageAvailable = false;
                            mExternalStorageWriteable = false;
                        }
                        File exst = Environment.getExternalStorageDirectory();
                        String exstPath = exst.getPath();

                        File fooo = new File(exstPath + "/MSDownloaderApp");
                        boolean success = fooo.mkdir();

                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lengthOfFile = conection.getContentLength();
                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(url.openStream(), 8192);

                        if(boolOverWriteDL) {
                        File fileExists = new File(fooo+"/"+fileName);
                            fileName = uri.getLastPathSegment();
                        }
                        else if(!boolOverWriteDL)
                        {
                            Random rand = new Random();
                            int randInt = rand.nextInt( 10000 );
                            fileName = String.valueOf(randInt) + fileName;
                            File fileExists = new File(fooo+"/"+fileName);
                        }



                        OutputStream output = new FileOutputStream(fooo + "/" + fileName);

                        // Output stream to write file
                        byte data[] = new byte[1024];

                        long total = 0;



                            while ((count = input.read(data)) != -1 && !cancelDL) {
                                total += count;
                                progressNumber = (int) (total * 100 / lengthOfFile);
                                sendMessage(progressNumber);
                                output.write(data, 0, count);
                            }
                        output.flush();
                        output.close();
                        input.close();
                    } catch (Exception e) {

                        Log.e("Error: ", "hui" + e.getMessage());
                        noValidURL(urlString);
                        if (cancelDL)
                            cancelDL = false;
                    }
                    if (cancelDL) {
                        cancelDL = false;
                    }
                }
            }).start();

        }


    //1. Wenn man Client-Service binden möchte --> Objekt erstellen durch Binder erweitert -> nun kann er 2 Dinge zusammenbinden
    public class MyLocaleBinder extends Binder{
    //2. LocalBinder returnt nur Referenz zur Mutterklasse (MyService), damit wir die Methoden in MyService nutzen können
        MyService getService(){
            return MyService.this;
        }
    }



}

