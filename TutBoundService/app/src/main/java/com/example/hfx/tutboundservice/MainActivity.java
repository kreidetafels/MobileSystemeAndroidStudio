package com.example.hfx.tutboundservice;
import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//Superclass --> Bindingclass
import com.example.hfx.tutboundservice.MyService.MyLocaleBinder;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * ToDo: Ordner umbennenen
 * */

//MainActivity versucht sich als Client an MyService zu binden
public class MainActivity extends AppCompatActivity {

    final static String ServLogTag = "ServiceLogTag";
    final static String MY_ACTION = "MY_ACTION";
    final static String INVALID_URL = "INVALID_URL";

    private MyService TutServiceObj;
    boolean isBound = false;
    private int i = 0;

    private IntentFilter receiveFilter;
    private TextView mLoadingText;

    private ProgressBar pgBar;
    private int mProgressStatus = 0;
    final static String NOTIFICATION_CHANNEL_ID = "id_product";
    private EditText editText;

    private CheckBox cBOverwrite;
    Button btnDownload;
    Button btnCancel;
    int notiID = 1337;


    private NotificationManager notiManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkCheckbox();

        pgBar = findViewById(R.id.pBDownload);
        editText = findViewById(R.id.eTDownloadLink);
        cBOverwrite = findViewById(R.id.cBOverwrite);
        btnDownload = findViewById(R.id.btnDownload);
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setEnabled(false);
        notiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i = new Intent(this, MyService.class);

        //Intent service, danach muss ein ServiceConnection-Objekt sein, danach Flag setzen
        bindService(i, TutServiceConnection, Context.BIND_AUTO_CREATE);
        verifyStoragePermissions(this);
        getApplicationName(getApplicationContext());


        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(INVALID_URL));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(MY_ACTION));

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(MY_ACTION)){
                String message = intent.getStringExtra(MY_ACTION);
                mProgressStatus = Integer.parseInt(message);
                pgBar.setProgress(mProgressStatus);

                if(mProgressStatus==100)
                {
                    Toast.makeText(context, getString(R.string.downloadDone), Toast.LENGTH_SHORT).show();
                    btnDownload.setEnabled(true);
                    mProgressStatus=0;
                    pgBar.setProgress(mProgressStatus);
                }
            }
            else if(intent.getAction().equals(INVALID_URL)){
                String message = intent.getStringExtra(INVALID_URL);
                invalidURL(message);
                btnCancel.setEnabled(false);
                mProgressStatus=0;
                pgBar.setProgress(mProgressStatus);
            }
        }
    };

    public void invalidURL(String _invalidURL){
        editText.setHint(R.string.invalidURL);
        btnDownload.setEnabled(true);
        Toast.makeText(MainActivity.this, getString(R.string.URLNotValid) + _invalidURL, Toast.LENGTH_LONG).show();
        notiManager.cancel(notiID);
    }

    public void startDownload(View v) {
        checkCheckbox();
        MyService my = new MyService();
        btnDownload.setEnabled(false);
        btnCancel.setEnabled(true);

        my.getDL(editText.getText().toString());

        Toast.makeText(this, "URL wird geöffnet: " + editText.getText().toString(), Toast.LENGTH_SHORT).show();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyService.MY_ACTION);

        //Start our own service
        Intent intent = new Intent(getApplicationContext(),
                com.example.hfx.tutboundservice.MyService.class);

        startService(intent);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);
        startNotification(contentIntent);
    }

    public void cancelDL(View v){
        mProgressStatus=0;
        pgBar.setProgress(mProgressStatus);

        btnDownload.setEnabled(true);
        btnCancel.setEnabled(false);

        MyService my = new MyService();
        Intent intent = new Intent(getApplicationContext(),
                com.example.hfx.tutboundservice.MyService.class);
        Toast.makeText(MainActivity.this, "DL abgebrochen", Toast.LENGTH_SHORT).show();
        my.cancelDL = true;
        startService(intent);
        notiManager.cancel(notiID);
    }

    public void checkCheckbox(){

        cBOverwrite = findViewById(R.id.cBOverwrite);

        cBOverwrite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MyService my = new MyService();

                //is chkIos checked?
                if (cBOverwrite.isChecked()) {
                    my.boolOverWriteDL = true;
                    Toast.makeText(MainActivity.this, getString(R.string.cBOverwriteYes), Toast.LENGTH_SHORT).show();
                }else {
                    my.boolOverWriteDL = false;
                    Toast.makeText(MainActivity.this, getString(R.string.cBOverwriteNo), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    public void startNotification(PendingIntent contentIntent){

        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_file_download_black_24dp) //your app icon
                .setBadgeIconType(R.drawable.ic_file_download_black_24dp) //your app icon
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getApplicationName(getApplicationContext()))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setNumber(1)
                .setColor(255)
                .setContentText(editText.getText().toString());
        nBuilder.setContentIntent(contentIntent).setAutoCancel(true).build();

        //Ab Android Oreo:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
        }
        Notification notification = nBuilder.build();
        notiManager.notify(notiID, notification);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        unbindService(TutServiceConnection);
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     * If the app does not has permission then the user will be prompted to grant permissions
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }



    //Was soll passieren, wenn connectet / disconnectet wird?
    private ServiceConnection TutServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            //Referenz auf MyLocaleBinder --> Access granten
            MyLocaleBinder binder = (MyLocaleBinder) service;

            //Nun: getService-Methode callen, um getCurrentTime nutzen zu können
            TutServiceObj = binder.getService();
            isBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };


}
