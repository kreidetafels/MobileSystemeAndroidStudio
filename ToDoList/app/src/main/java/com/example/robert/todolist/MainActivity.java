package com.example.robert.todolist;

import android.app.ListActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String[] LISTENELEMENTE = {"ToDo-App bauen","fasfjafsjfas","So viel zu tun...", "Hallo", "ToDo-App dynamisch Einträge generieren lassen", "Einträge löschen können", "Einträge hinzufügen können", "Schriftgröße ändern"};

        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, LISTENELEMENTE));
        ListView lv = getListView();
        lv.setVerticalScrollBarEnabled(true);
        lv.setOnItemClickListener(new ListView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int position, long id)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Ihre Wahl: " + ((TextView)v).getText().toString());
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

}
