package com.example.robert.helloworld;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }
    //Befehlsschalter bei jedem Klick MSG an Aktivitätsobjekt schickt, indem er dessen Methode onClick() aufruft.
    @Override
    public void onClick(View v) {
        //Ref-Var des Typs EditText um Z. / N. anzusprechen
        EditText etZaehler = (EditText) findViewById(R.id.zaehler);
        EditText etNenner = (EditText) findViewById(R.id.nenner);

        //etZ / etN per getText-Methode Inhalt anfordern
        String sz = etZaehler.getText().toString();
        String sn = etNenner.getText().toString();
        int z;
        int n;

        if(sz.length() == 0 || sn.length() == 0)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Bitte Zähler & Nenner eintragen");
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else {
            z = Integer.parseInt(sz);
            n = Integer.parseInt(sn);

            //Euklid. Algo fuer ggT
            if (z != 0) {
                int rest;
                int ggt =  Math.abs(z);
                int divisor = Math.abs(n);
                do {
                    rest = ggt % divisor;
                    ggt = divisor;
                    divisor = rest;
                } while (rest > 0);
                z /= ggt;
                n /= ggt;
            } else
                n = 1;

            etZaehler.setText(Integer.toString(z));
            etNenner.setText(Integer.toString(n));
        }





    }

}
