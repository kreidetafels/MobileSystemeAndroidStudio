package com.example.hfx.tutsharedpref;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String FILENAME = "PreferencesFilename";
    private static final String VAL_KEY = "ValueKey";
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        SharedPreferences sharedPrefs = getSharedPreferences(FILENAME, 0);
        editText.setText(sharedPrefs.getString(VAL_KEY, "Standardwert"));


    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences sharedPref = getSharedPreferences(FILENAME, 0);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(VAL_KEY, editText.getText().toString());
        editor.commit();
    }
}
