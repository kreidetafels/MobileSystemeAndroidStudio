package com.example.robert.mswegga;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

/* Created by Robert Neumann on 30.04.2018.
 * Matrikel-Nr.: 15589
 * ToDo:
 */

public class SnoozeActivity extends AppCompatActivity {
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snooze);
        playSound(getApplicationContext(),getAlarmUri());
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }

    private void playSound(Context context, Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    //Hol Alarm-Sound, ansonsten versuch es mit Notification, ansonsten Ringtone
    private Uri getAlarmUri() {
        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }


    public void snoozeAlarm(View v){
        int snoozeTime = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("snoozeTime","10"));

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, SnoozeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+(snoozeTime*60*1000), pendingIntent);
        mMediaPlayer.stop();
        Toast.makeText(this, "Snooze-Zeit wurde auf "+snoozeTime+" Minuten gestellt", Toast.LENGTH_LONG).show();
        finish();
    }

    public void cancelAlarm(View v){
        mMediaPlayer.stop();
        Toast.makeText(this,"Wecker deaktiviert", Toast.LENGTH_LONG).show();
        finish();
    }
}
