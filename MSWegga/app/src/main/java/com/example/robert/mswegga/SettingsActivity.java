package com.example.robert.mswegga;

/* Created by Robert Neumann on 30.04.2018.
 * Matrikel-Nr.: 15589
 * ToDo:
 */
import android.preference.PreferenceActivity;
import android.os.Bundle;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sharedprefs);
    }
}