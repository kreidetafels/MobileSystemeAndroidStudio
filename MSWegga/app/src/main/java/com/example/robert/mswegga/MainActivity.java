package com.example.robert.mswegga;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

/* Created by Robert Neumann.
 * Matrikel-Nr.: 15589
 * Hilfe von R. Pfeiffer
 * */

public class MainActivity extends AppCompatActivity {

    public static final int notiID = 1337;
    public static final int TagInMS = 86400000;

    private TimePicker timePicker;
    private Button btnSet;
    private Button btnCancel;
    private PendingIntent pendingIntent;
    private PendingIntent pi;
    private AlarmManager alarmManager;
    private NotificationManager notiManager;
    final static String NOTIFICATION_CHANNEL_ID = "id_product";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timePicker = findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        btnSet = findViewById(R.id.btnSetAlarm);
        btnCancel = findViewById(R.id.btnCancel);
        notiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, SnoozeActivity.class);
        pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.MenuItemSettings: {
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setAlarm(View v) {
        alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + getTimePickerContentMillis(), pi);
        String weckzeit = ("Der Wecker wurde auf " + timePicker.getHour() + ":" + timePicker.getMinute() + " gestellt.");

        //Start our own service
        Intent intent = new Intent (this, MainActivity.class);
        intent.addFlags (Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pend = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        startNotification(weckzeit, pend);

        Toast.makeText(this, "Neue Weckzeit wurde auf " + timePicker.getHour() + ":" + timePicker.getMinute() + " gestellt.", Toast.LENGTH_LONG).show();
    }

    public void startNotification(String _weckzeit, PendingIntent contentIntent){

        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp) //your app icon
                .setBadgeIconType(R.drawable.ic_notifications_active_black_24dp) //your app icon
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getApplicationName(getApplicationContext()))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setNumber(1)
                .setColor(255)
                .setContentText(_weckzeit);
        nBuilder.setContentIntent(contentIntent).setAutoCancel(true).build();

        //Ab Android Oreo:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            CharSequence name = "Weckzeit";
            String description = "Weckzeit gestellt";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
        }
        Notification notification = nBuilder.build();
        //     NotificationManager notiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notiManager.notify(notiID, notification);
    }

    public void cancelAlarm(View v) {
        alarmManager.cancel(pi);
    }

    /**
     * @return Weckzeit in MS
     */

    public long getTimePickerContentMillis() {
        Calendar now = Calendar.getInstance();
        long daytimeMillis = now.get(Calendar.HOUR_OF_DAY) * 3600000 + now.get(Calendar.MINUTE) * 60000 + now.get(Calendar.SECOND) * 1000;
        long timepickerMillis = ((timePicker.getHour() * 60 * 60 * 1000) + ((timePicker.getMinute()) * 60 * 1000));
        if (timepickerMillis <= daytimeMillis)
            timepickerMillis += TagInMS;
        return (timepickerMillis-daytimeMillis);
    }
}